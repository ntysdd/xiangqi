use std::env;
use std::process;

mod bitboard;
mod bitboard_tables;
mod board;
mod color;
mod move_gen;
mod moves;
mod piece;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("需要命令行参数");
        process::exit(1);
    }
    let b = board::parse_fen_str(&args[1]);
    let mut b = b.unwrap();
    let mut mg = move_gen::MoveGen::new(&mut b);
    while let Some(m) = mg.next() {
        println!("{}", m.human_string(mg.get_board()));
    }
}
