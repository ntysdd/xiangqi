#[repr(i8)]
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Color {
    BLACK = -1,
    RED = 1,
}

impl Color {
    pub fn color_index(self) -> usize {
        match self {
            Color::BLACK => 0,
            Color::RED => 1,
        }
    }

    pub fn other_side(self) -> Color {
        match self {
            Color::BLACK => Color::RED,
            Color::RED => Color::BLACK,
        }
    }
}

pub const COLORS: [Color; 2] = [Color::RED, Color::BLACK];
